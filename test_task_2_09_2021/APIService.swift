//
//  APIService.swift
//  test_task_2_09_2021
//
//  Created by Kadir Kadyrov on 02.09.2021.
//

import Foundation

class APIService {
    static let shared = APIService()
    
    private let URL_SCHEME = "https"
    private let URL_BASE = "2llctw8ia5.execute-api.us-west-1.amazonaws.com"
    func getTracks(
        onSuccess: @escaping (Links) -> Void,
        onError: @escaping (String) -> Void
    ) {
        var urlComponents = URLComponents()
    
        urlComponents.scheme = "\(URL_SCHEME)"
        urlComponents.host = "\(URL_BASE)"
        urlComponents.path = "/prod"
        
        let request = NSMutableURLRequest(url: urlComponents.url!,
            cachePolicy: .useProtocolCachePolicy,
        timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                
                do {
                    if response.statusCode == 200 {
                        print(data)
                        let tracks = try JSONDecoder().decode(Links.self, from: data)
                        
                        onSuccess(tracks)
                    } else {
                        let err = try JSONDecoder().decode(String.self, from: data)
                        onError(err)
                    }
                }
                catch {
                    onError(error.localizedDescription)
                }
            }
            
        }
        task.resume()
    }
}
