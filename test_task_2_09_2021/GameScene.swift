//
//  GameScene.swift
//  test_task_2_09_2021
//
//  Created by Kadir Kadyrov on 02.09.2021.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    typealias OnWin = () -> Void
    typealias OnLose = () -> Void
    
    private var timerLabel : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    private var touchView : SKShapeNode?
    private var counter: Int = 10
    private var levelTimerValue: Double = 7.0 {
        didSet {
            timerLabel?.text = "\(Double(round(10*levelTimerValue)/10))"
        }
    }
    
    public var onWin: OnWin?
    public var onLose: OnLose?
    
    override func didMove(to view: SKView) {
        setup()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let touchPosition = touch!.location(in: self)
        let touchedNodes = self.nodes(at: touchPosition)
        
        for touchedNode in touchedNodes {
            if let name = touchedNode.name {
                if name == "touchView" {
                    if counter == 10 {
                        setupTimer()
                    }
                    counter -= 1
                    if counter == 0 {
                        onWin?()
                    }
                    undateTouchViewPosition()
                }
            }
        }
//
//        if let label = self.timerLabel {
//            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
//        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {}
    
    override func update(_ currentTime: TimeInterval) {}
}

private extension GameScene {
    
    func setup() {
        setupTouchView()
        setupTimerLabel()
        setupValues()
    }
    
    func setupTouchView() {
        self.touchView = .init(
            rectOf: .init(
                width: 64,
                height: 64
            ),
            cornerRadius: 64 * 0.3
        )
        
        if let touchView = self.touchView {
            undateTouchViewPosition()
            touchView.name = "touchView"
            touchView.fillColor = .red
            self.addChild(touchView)
        }
    }
    
    func setupTimerLabel() {
        self.timerLabel = self.childNode(withName: "//timerLabel") as? SKLabelNode
        if let label = self.timerLabel {
            label.alpha = 1.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
            label.position = .init(x: 0, y: 0)
        }
    }
    
    func setupValues() {
        counter = 10
        levelTimerValue = 7
    }
    
    func setupTimer() {
        let wait = SKAction.wait(forDuration: 0.1)
        let block = SKAction.run({ [unowned self] in
            
            if self.levelTimerValue > 0 {
                self.levelTimerValue -= 0.1
                self.levelTimerValue = max(self.levelTimerValue, 0.0)
            } else {
                onLose?()
                self.removeAction(forKey: "levelTimer")
            }
        })
        let sequence = SKAction.sequence([wait,block])
        
        run(
            SKAction.repeatForever(sequence),
            withKey: "levelTimer"
        )
    }
    
    func undateTouchViewPosition() {
        if let touchView = self.touchView {
            touchView.position = .init(
                x: CGFloat.random(in: -size.width / 2 ... size.width / 2),
                y: CGFloat.random(in: -size.height / 2 + 150 ... size.height / 2 - 150)
            )
        }
    }
}
