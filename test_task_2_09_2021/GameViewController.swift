//
//  GameViewController.swift
//  test_task_2_09_2021
//
//  Created by Kadir Kadyrov on 02.09.2021.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    typealias OnTouchBackButton = () -> Void
    typealias OnWin = () -> Void
    typealias OnLose = () -> Void

    public var onTouchBackButton: OnTouchBackButton?
    public var onWin: OnWin?
    public var onLose: OnLose?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showGameView()
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    @IBAction func backButtonTouch(_ sender: Any) {
        onTouchBackButton?()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

private extension GameViewController {
    func showGameView() {
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: "GameScene") as? GameScene {
                scene.scaleMode = .aspectFill
                
                scene.onLose = {
                    self.onLose?()
                }
                
                scene.onWin = {
                    self.onWin?()
                }
                
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }
}
