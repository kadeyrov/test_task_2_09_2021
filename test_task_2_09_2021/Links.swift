//
//  Links.swift
//  test_task_2_09_2021
//
//  Created by Kadir Kadyrov on 02.09.2021.
//

import Foundation

struct Links: Codable {
    let winnerUrl: String?
    let loserUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case winnerUrl = "winner"
        case loserUrl = "loser"
    }
}
