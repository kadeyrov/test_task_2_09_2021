//
//  StartViewController.swift
//  test_task_2_09_2021
//
//  Created by Kadir Kadyrov on 02.09.2021.
//

import UIKit
import SafariServices

class StartViewController: UIViewController {

    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var startButton: UIButton!
    private var links: Links?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        APIService.shared.getTracks { links in
            self.links = links
        } onError: { error in
            debugPrint(error)
        }
        
        let defaults = UserDefaults.standard
        if let isTutorialShowed = defaults.object(forKey: "isTutorialShowed") as? Bool,
           isTutorialShowed {
            
        } else {
            showTutorial()
            defaults.set(true, forKey: "isTutorialShowed")
        }
    }

    
    @IBAction func hideTutorialView(_ sender: Any) {
        tutorialView.isHidden = true
    }
    
    @IBAction func startButtonTouch(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(identifier: "GameViewController") as? GameViewController {
            
            vc.modalPresentationStyle = .overCurrentContext
            
            vc.onTouchBackButton = {
                vc.dismiss(animated: true)
            }
            
            vc.onWin = {
                vc.dismiss(animated: true)
                
                guard let stringUrl = self.links?.winnerUrl,
                    let url = URL(string: stringUrl) else {
                    return
                }
                
                let webVC = SFSafariViewController(
                    url: url,
                    configuration: .init()
                )
                
                self.present(webVC, animated: true)
            }
            
            vc.onLose = {
                vc.dismiss(animated: true)
                
                guard let stringUrl = self.links?.loserUrl,
                    let url = URL(string: stringUrl) else {
                    return
                }
                
                let webVC = SFSafariViewController(
                    url: url,
                    configuration: .init()
                )
                
                self.present(webVC, animated: true)
            }
            
            self.present(vc, animated: true)
        }
    }
}

private extension StartViewController {
    
    func setup() {
        setupTutorialView()
    }
    
    func setupTutorialView() {
        tutorialView.layer.cornerRadius = 15.0
        tutorialView.layer.shadowColor = UIColor.systemGray3.cgColor
        tutorialView.layer.shadowRadius = 3.0
        tutorialView.layer.borderWidth = 1.0
        tutorialView.layer.borderColor = UIColor.white.cgColor
    }
    
    func showTutorial() {
        tutorialView.isHidden = false
    }
}
